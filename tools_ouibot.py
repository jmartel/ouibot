import datetime
import json
import requests

################################################################################
#
################################################################################
def print_json(data):
	print(json.dumps(data,indent=4))

################################################################################
#
################################################################################
def convert_date(date):
	date = datetime.datetime.strptime(date, '%Y-%m-%dT%H:%M:%S').strftime('%A %d/%m %H:%M')
	return (date)

################################################################################
# Return a string containing current date as MM/DD/YYYY HH:MM:SS
################################################################################
def current_date():
	return (datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))

################################################################################
# Use slack hook to send message to jmartel
################################################################################
def contact(message):
	url = 'https://hooks.slack.com/services/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'
	header = { 'Content-type': 'application/json' }
	datas = { 'text': message }
	response = requests.post(url, headers=header, json=datas)
	if (response.status_code != 200):
		print ("Cannot use slack webhook")

################################################################################
# Send slack message and print message on stdout
################################################################################
def contact_print(message):
	contact(message)
	print(message)

################################################################################
# Write value in a log file
################################################################################
def write_log(value):
	fdw = open('log.log', 'a')
	fdw.write(value)
	if (value[-1] != '\n'): fdw.write("\n")
	fdw.close()

from signal import signal, SIGINT

def handler(signal_received, frame):
	print('Exiting ouibot')
	exit (0)

signal(SIGINT, handler)