################################################################################
# Load username and password vaiables from the .credential file, used to log in
# Errors are considered as fatal
################################################################################
if (sum(1 for line in open('./.credentials')) == 2):
	exec(open("./.credentials").read())
else:
	print ("Error in credentials") ; exit (1)

################################################################################
# Used user and passwd to create form to connect to oui.sncf website
################################################################################
def logging(user, passw):
	global userId
	global session;

	params = 'lang=fr&login=' + user + '&password=' + passw
	header = {
		'Host': 'www.oui.sncf',
		'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:65.0) Gecko/20100101 Firefox/65.0',
		'Accept': '*/*',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate, br',
		'Referer': 'https://www.oui.sncf/sites/all/modules/custom_addons/vsct_feature_canvas/theme/canvas-proxy.html?898c7c160fd08b277abc7625c4a49b91',
		'Content-Type': 'application/x-www-form-urlencoded',
		'Content-Length': '55',
		'DNT': '1',
		'Connection': 'keep-alive',
		"Cookie" : 'x-vsc-dialog-id=cbd4b956-8153-403b-9c0a-6e67fc104c2c; userId-token=""; JSESSIONID=87443B8FAABE011E0C97516CB93DFB14; va_ic=655d1c864f568d9eaf3b2121d02a4b6a75439421%3A1571352505672; country_code=FR; x-vsc-correlation-id=d6879762-2ea3-4f42-a948-fb0f905a7d34; VSL_city=SDN_PRD9; ABTasty=uid%3D19101800480925773%26fst%3D1571352489940%26pst%3Dnull%26cst%3D1571352489940%26ns%3D1%26pvt%3D4%26pvis%3D4%26th%3D135889.0.8.8.1.1.1571352489971.1571352515047.1_492017.-1.0.0.0.0.1571352512123.1571352512123.1_493724.619387.4.4.1.1.1571352490792.1571352515063.1; ABTastySession=sen%3D27__referrer%3Dhttps%3A//www.google.com/__landingPage%3Dhttps%3A//www.oui.sncf/bot__referrerSent%3Dtrue; has_js=1; va_iv=44be86a2-6237-4e96-8c71-74a2625e2bb5; CCLSESSION=ccltulp73; dydu.context=eyJib3RJRCI6ImMwZGYzMzA0LWU5YmMtNGZhNi1hNDQxLTZhNGUwZjZlNjIyZiIsImxhbmd1YWdlIjoiZnIiLCJsYXN0VXBkYXRlIjoxNTcxMzUyNDkxMDUwLCJjb250YWN0cyI6W10sImlkIjoiIiwib2xkSUQiOiIiLCJoaWRkZW5JRCI6IiIsInNwYWNlIjoiRnJhbmNlIn0%3D; dydu.lastvisitfor=IjIwMTktMTAtMTdUMjI6NDg6MTEuMzA2WiI%3D; dydu.push=eyJyX3BhZ2VzVmlld2VkIjp7ImNvdW50IjoyfSwicl9sYXN0cGFnZWxvYWRlZCI6e319; vsct_policy_compliance=1; va_stopTagsCategories=none; AMCV_F3F4148954F730780A4C98BC%40AdobeOrg=2096510701%7CMCIDTS%7C18187%7CMCMID%7C77477362285147270011909907754569500020%7CMCAID%7CNONE%7CMCOPTOUT-1571359692s%7CNONE%7CvVersion%7C2.0.0; va_first_visit=1; va_previousPageName=HomeBot; va_s_getNewRepeat=1571352516683-New; s_ecid=MCMID%7C77477362285147270011909907754569500020; AMCVS_F3F4148954F730780A4C98BC%40AdobeOrg=1; ck_dir=1; c_m=www.google.comNatural%20Search; ck_chStak=%5B%5B%27SEO%2520Inconnu%27%2C%271571352492985%27%5D%5D; s_cc=true; va_epm=1; _cs_ex=1; _cs_c=1; s_sq=voyagessncfcomprod%3D%2526c.%2526a.%2526activitymap.%2526page%253DHomeBot%2526link%253DMe%252520connecter%2526region%253Dvsc-menu-ccl-login-form%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253DHomeBot%2526pidt%253D1%2526oid%253Dfunction_o%252528%252529%25257B%25257D%2526oidt%253D2%2526ot%253DSUBMIT; kameleoonVisitorCode=_js_mn3wftzig662uj14; VSCAuth=""; isAuthentified=""; user-askForAutologin=""; userId-firstName=""; CCLAGGSESSION=cclmonp62agg; vscsas=39; VSBSESSION=vsbsaip91; VSLPRD9Session=9893352417D12D34B0EB79510FA71232; x-vsc-app-version-3000=ACTIVATED; AGGREGSESSION=vslfrep92agr; RT="r=https%3A%2F%2Fwww.oui.sncf%2F&hd=1571352514623"; va_iv=44be86a2-6237-4e96-8c71-74a2625e2bb5; rfrr=HomeBot_Menu-CompteClient_FORM-; merchTrck=%23',
	}
	response = session.post("https://www.oui.sncf/espaceclient/authentication/flowSignIn", params=params)
	if (response.status_code == 200):
		print("authentification successfull")
	else:
		print("error during authentification : " + str(response.status_code))
	userId = "44be86a2-6237-4e96-8c71-74a2625e2bb5"
	print(userId)
	return (response)
