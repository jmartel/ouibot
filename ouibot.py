#!/usr/local/bin/python3

import argparse
import requests, json
import time
import datetime
import sys

from tools_ouibot import *

session = requests.Session()

################################################################################
# Define parser object used to check command line arguments and store values in
# different variables used in programm, launch ./ouibot.py -h to see details
################################################################################
def parser():
	global searches
	global args
	global tgvmax, surname, name, mail, birthdate

	parser = argparse.ArgumentParser()
	parser.add_argument("--no_buy", "-b", help="Do not try to buy ticket if any valid ticket is find, do not loop", action="store_true")
	parser.add_argument("--no_send", "-s", help="Do not send messages", action="store_true")
	parser.add_argument("--one", "-1", help="do not loop", action="store_true")
	parser.add_argument("--wait", "-w", type=int, help="Time beetween two series of requests", action="store", default=60)
	parser.add_argument("--verbose", "-v", type=int, help="Verbose mode", action="store", default=0)

	parser.add_argument("--tgvmax", type=str, help="TGVmax number used to reserve", action="store", default=tgvmax)
	parser.add_argument("--surname", type=str, help="Surname used to reserve", action="store", default=surname)
	parser.add_argument("--name", type=str, help="Nname used to reserve", action="store", default=name)
	parser.add_argument("--mail", type=str, help="Mail used to reserve", action="store", default=mail)
	parser.add_argument("--birthdate", type=str, help="Birthdate used to reserve with format : DD/MM/YYYY", action="store", default=birthdate)
	parser.add_argument('requests', type=str, nargs='+', help='List of requests to store and execute')
	args = parser.parse_args()
	searches = args.requests

################################################################################
# Read a file to get informations about reserver, by default this file is
# .crendtials. See .credentials_model to see how to format it
################################################################################
def get_credential_line(fdr):
	try :
		line = fdr.readline()
	except :
		print("Credentials file not well formated")
		exit ()
	i = line.find(':')
	if (i != - 1):
		return (line[i + 1:].strip())
	else :
		return (line.strip())

def read_credentials(filename=".credentials"):
	global tgvmax, surname, name, mail, birthdate
	try :
		fdr = open(filename, "r")
	except :
		print("Cannot open .credentials files")
		return
	tgvmax = get_credential_line(fdr)
	surname  = get_credential_line(fdr)
	name = get_credential_line(fdr)
	mail = get_credential_line(fdr)
	birthdate = get_credential_line(fdr)
	fdr.close()

################################################################################
# Send a request to ouibot, simulating a message containing value
# It return a response object if no failure status were returned,
# else it returns none
################################################################################
def request_bot(value, log_answer=False):
	datas = {
		"authentificated" : "false",
		"items" : [ { "type" : "text", "value" : value } ],
		"recipientId" : "bot",
		"version" : "2",
		"userId" : "",
		"build" : "",
	}
	content_len = len(json.dumps(datas))
	datas = json.dumps(datas)
	headers= {
		'Host': 'bot.vsct.fr',
		'Accept': '*/*',
		'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
		'Accept-Encoding': 'gzip, deflate, br',
		'Connection': "keep-alive",
		'Content-lenght': content_len,
		'DNT' : 1,
		'TE' : 'Trailers',
		'Access-Control-Request-Method': 'POST',
		'Access-Control-Request-Headers': 'content-type',
		'Referer':' https://www.oui.sncf/bot',
		'Origin': 'https://www.oui.sncf',
		'Connection': 'keep-alive'
	}
	try :
		response = session.post('https://bot.vsct.fr/rest/rest-vsc', data=datas)
		if (args.verbose >= 1):	print(value + " : " + str(response.status_code))
		if (args.verbose >= 2) : print_json(json.loads(response.text))
		try :
			if (args.verbose >= 1): print(response.json()["messages"][0]["items"][0]["value"])
		except :
			pass
	except :
		print("An error occured during request : " + value)
		print("Error : " + str(sys.exc_info()[0]))
		if (log_answer)	:
			write_log(current_date())
			write_log("*" * 80 + "\n")
			write_log("An error occured during request : " + value)
			write_log("Error : " + str(sys.exc_info()[0]))
			#write_log("*" * 80 + "\n")
		return (None)
	if (log_answer):
		write_log(response.json()["messages"][0]["items"][0]["value"])
	if (response.status_code == 200):
		return (response)
	else:
		print("ERRROR : bot request : " + str(response.status_code) + " (" + value + ")")
		return (None)

################################################################################
# Look in server response to print (and send) human readable datas on valid
# tickets found. It check that price is equal to 0, to filter non tgvmax tickets
# It return an int, contaning index of the first valid ticket. If non free ticket
# was found, it returns -1.
################################################################################
def show_tickets(value, response):
	tickets = response.json()["messages"][1]["items"][:]
	buffer = "Available tickets for : " + value + "\n"
	index = -1
	for ticket in tickets:
		date = convert_date(ticket["data"]["departure_date"])
		if (ticket["data"]["price"] != 0):
			continue
		buffer += "\t" + str(date) + "\n"
		if (index == -1):
			index = tickets.index(ticket)
	if (args.no_send):
		print (buffer)
	else :
		contact_print(buffer)
	return (index)

################################################################################
# Try to reserver ticket designated by index int. If response to "reserver"
# request contain choices (suggestions), it mean that there were a problem during
# reservation and it show error message.
# Return 1 if buying is considered as successfull : success detected, or tickets
# limit had been reached (total or on the same day).
################################################################################
def buy_ticket(response, value, index):
	global args
	date = convert_date(response.json()["messages"][1]["items"][index]["data"]["departure_date"])
	buffer = "Trying to reserve ticket : " + date + "\n"
	write_log(current_date() + "\n" + buffer)
	response = request_bot(str(index + 1), log_answer=True)
	response = request_bot(args.tgvmax, log_answer=True)
	response = request_bot(args.name, log_answer=True)
	response = request_bot(args.surname, log_answer=True)
	response = request_bot(args.birthdate, log_answer=True)
	response = request_bot(args.mail, log_answer=True)
	response = request_bot("Continuer", log_answer=True)
	response = request_bot("Valider mon voyage", log_answer=True)
	choices = response.json()["messages"][0]["choices"]
	try :
		msg = response.json()["messages"][0]["items"][0]["value"]
	except :
		print("Bad message received (Timeout ?)")
		return (0)
	if (msg.find("2 billets TGVmax") != -1):
		ret = 1
		buffer += "Un autre ticket a deja ete reserve a cette date"
	elif (msg.find("👌") != -1):
		ret = 1
		buffer += "Ticket reserve avec succes"
	elif (msg.find("6 TGVmax en même temps") != -1):
		ret = 1
		buffer += "Maximum de tickets simultanes atteint"
	else:
		buffer += "Error when reserving ticket : \n\t" + msg + "\n"
		ret = 0
	if (args.no_send):
		print(buffer)
	else :
		contact_print(buffer)
	write_log("\n")
	return (ret)

################################################################################
# Call ouibot with the given request. If any ticket were given in the answer
# it check if any valid ticket was present, by calling show_tickets.
# If not it return an false, else if buy is true it try to buy ticket designated
# by index, and it return 1, even if buy failed.
# If any train was given, it return False.
################################################################################
def is_ticket_available(value):
	global args, empty_msg

	request_bot("restart")
	response = request_bot(value)
	if (response == None):
		return (0)
	try :
		msg = response.json()["messages"][0]["items"][0]["value"]
		empty_msg = 0
	except :
		print("Cannot find message in is_ticket_available function")
		empty_msg += 1
		return (0)
	# print(msg)
	if (msg.find("voici les TGVmax") != -1):
		index = show_tickets(value, response)
		if (index == -1):
			return (0)
		if (args.no_buy):
			return (1)
		return (buy_ticket(response, value, index))
	else:
		index = msg.find(" pour le")
		if (index != -1):
			print(value + " : " + msg[:index])
		else:
			print (value + " : No trains available")
		return (0)

################################################################################
# While searches list is not empty, it try to find any valid tickets for every
# requests.
# If valid ticket had been found request is deleted, even if it had not been
# bought or buying process failed
################################################################################
def main(searches):
	global args, empty_msg
	request_bot("restart")
	request_bot("tgvmax")
	if (args.no_buy):
		for search in searches:
			is_ticket_available(search)
		exit (0)
	empty_msg = 0
	while (searches):
		if (empty_msg > 10):
			exit (1)
		to_remove = []
		for search in searches:
			ret = is_ticket_available(search)
			if (ret):
				to_remove.append(search)
		for str in to_remove:
			searches.remove(str)
		if (args.one):
			break
		if (len(searches) > 0):
			time.sleep(args.wait)

read_credentials(".credentials")
parser()
main(searches)
