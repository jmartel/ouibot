import io
import sys
import getpass
import time

import stem.process
import requests, json

from stem.util import term
from stem.control import Controller
from stem import Signal

from tor_ouibot import ouibot, ouibot_read_credentials, ouibot_parser

SOCKS_PORT = 7000
CONTROL_PORT = 7001

def query(tor):
	"""
		Uses pycurl to fetch a site using the proxy on the SOCKS_PORT.
		"""
	proxies = {
		'http': 'socks5://localhost:' + str(SOCKS_PORT),
		'https': 'socks5://localhost:' + str(SOCKS_PORT)
	}
	session = requests.Session()
	if (tor):
		response = session.get('https://ifconfig.me/ip', proxies=proxies)
	else :
		response = session.get('https://ifconfig.me/ip')
	print(response.status_code)
	print(response.text)


# Start an instance of Tor configured to only exit through Russia. This prints
# Tor's bootstrap information as it starts. Note that this likely will not
# work if you have another Tor instance running.

def print_bootstrap_lines(line):
	if "Bootstrapped " in line:
		print(term.format(line, term.Color.BLUE))

def change_ip():
	try:
		controller = Controller.from_port(address = '127.0.0.1', port = CONTROL_PORT)
	except :
		print("Unable to connect to tor on port " + str(CONTROL_PORT) + ": %s" % exc)
		sys.exit(1)

	try:
		controller.authenticate("16:6608DD757259DC3C609E33A210ECB654DA65979539BA22FA897160E407")
	except stem.connection.PasswordAuthFailed:
		print("Unable to authenticate, password is incorrect")
		sys.exit(1)
	except stem.connection.AuthenticationFailure as exc:
		print("Unable to authenticate: %s" % exc)
		sys.exit(1)

	controller.signal(Signal.NEWNYM)
	time.sleep(controller.get_newnym_wait())
	controller.close()

def init_tor_connection():
	print(term.format("Starting Tor:\n", term.Attr.BOLD))
	tor_process = stem.process.launch_tor_with_config(
		config = {
			'SocksPort': str(SOCKS_PORT),
			'ControlPort': str(CONTROL_PORT),
			'HashedControlPassword': '16:3FA3B47CCEC8740A607DAF32FC1BB7913926C7A600ED828FAC47863E51' },
		init_msg_handler = print_bootstrap_lines)
	return (tor_process)

def main():
	global searches

	tor_process = init_tor_connection()
	ret = 0
	while (ret == 2 or ret == 0):
		ret = ouibot(searches)
		if (ret == 2):
			change_ip()
	tor_process.kill()  # stops tor